# RBAC Poc
There Entities User, Group, Collection and Item and 3 types of roles General Manager, Group Manager and Regular.

**Regular Users** can only READ the entities associted to it.

**Group Manager** can CRUD all entities realted to the groups they owned.

**General Manager** can CRUD every thing.

Solution consists of 2 phase authorization middlewares:

[1] First one used to eliminate the non-used roles for the resource and re-scope the access to the roles that the user already have.

[2] Second one checks the filtered roles regarding to the action which the user need to dispatch to the resource.

* `rbac.js` Contains the association between the resources, roles and the actions*

*There is a basic E2E test to simulate the simple flow.*


## Installation

```
$ docker-compose up -d
```

The main api listens on port `3200` and prefix `/api`
```
http://localhost:3200/api
```



## Build & Run Tests
using yarn
```
$ yarn
$ yarn test
```

using npm
```
$ npm install
$ npm run test
```


## Docs

Swagger is available for all resources on `/docs`
```
http://localhost:3200/docs
```
