import express from 'express'
import { authRouter } from '../lib/modules/auth/auth.router'
import { collectionRouter } from '../lib/modules/collection/collection.router'
import { groupRouter } from '../lib/modules/group/group.router'
import { itemRouter } from '../lib/modules/item/item.router'
import { userRouter } from '../lib/modules/user/user.router'

const router = express.Router()


router.use('/auth', authRouter)
router.use('/user', userRouter)
router.use('/group', groupRouter)
router.use('/collection', collectionRouter)
router.use('/item', itemRouter)

export {
    router
}