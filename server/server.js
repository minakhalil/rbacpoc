import { app } from './app'
import { mongoConnectionInit, seedGeneralManager } from '../lib/utils/database'
import { MainConfig } from '../lib/config'
import { logger } from '../lib/utils/logger'

mongoConnectionInit().then(async () => {
    seedGeneralManager()
    app.listen(MainConfig.port || 3000, '0.0.0.0', () => logger.info(`Server is running on port ${MainConfig.port}`))
})