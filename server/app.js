import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'
import { globalErrorHandler } from '../lib/middlewares/error'
import { router } from './router'
import swaggerUi from 'swagger-ui-express'
import { swaggerSpecOptions } from '../lib/utils/swagger'
import swaggerJSDoc from 'swagger-jsdoc'
import { MainConfig } from '../lib/config'



const app = express()
const appRouter = express.Router()



/**
 * General Middlewares
 */

app.use(cors())
app.use(helmet())
app.use(bodyParser.json());



/**
 * Docs
 */

app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(swaggerJSDoc(swaggerSpecOptions), {
        explorer: true,
        customCss: '.swagger-ui .topbar { display: none }',
        customSiteTitle: MainConfig.serviceName
    })
)

appRouter.use(
    "/api",
    router)
app.use(appRouter)


app.use(globalErrorHandler)


export { app }
