export const ACTIONS = {
    READ_ONE: "read_one",
    READ_ALL: "read_all",
    CREATE: "create",
    UPDATE: "update",
    DELETE: "delete"
}

export const RESOURCES = {
    USER: "user",
    GROUP: "group",
    COLLECTION: "collection",
    ITEM: "item"
}

export const USER_TYPES = {
    REGULAR: "regular",
    GROUP_MANAGER: "group_manager",
    GENERAL_MANAGER: "manager"
}