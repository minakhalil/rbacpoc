import Collection from './collection.model'

export const createCollection = async (name, gid) => {
    return await Collection.create({
        name,
        group: gid
    })
}

export const getCollectionById = async (id) => {
    return await Collection.findById(id)
}

export const getAllCollections = async (_scope) => {
    if (_scope === undefined)
        return await Collection
            .find()
            .populate('group')
            .exec()

    return await Collection
        .find({
            group: _scope
        })
        .populate('group')
        .exec()
}

export const updateCollection = async (id, params) => {
    return await Collection.findOneAndUpdate({ _id: id }, {
        ...params
    })
}

export const deleteCollection = async (id) => {
    return await Collection.findByIdAndDelete(id)
}



export const deleteCollectionsByGroupId = async (id) => {
    return await Collection.deleteMany({ group: id })
}