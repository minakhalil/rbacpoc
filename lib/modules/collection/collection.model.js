import mongoose from 'mongoose'

const collectionSchema = new mongoose.Schema({

    name: {
        type: String,
        trim: true
    },
    group: {
        type: mongoose.Schema.ObjectId,
        ref: 'Group'
    }

}, {
    timestamps: true
})

export default mongoose.model('Collection', collectionSchema)