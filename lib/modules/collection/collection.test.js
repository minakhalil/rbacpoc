import request from 'supertest'
import { app } from '../../../server/app'


describe("Testing Add new collection", () => {

    it('returns a 201 on successful collection creation', async () => {

        await request(app)
            .post('/api/collection/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'C1',
                group: global.mocksObjects.group
            })
            .expect(201)
    })


    it('returns a 422 with codes 411 with an missing name', async () => {

        await request(app)
            .post('/api/collection/')
            .set({ Authorization: global.gmAccessToken })
            .send({

            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(411))

    })


    it('returns a 422 with codes 412 with an missing group', async () => {

        await request(app)
            .post('/api/collection/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: "C1"
            })
            .expect(422)
            .expect(res => {
                expect(res.body.code).toEqual(412)
            })

    })

    it('returns a 404 for non exisiting group', async () => {

        await request(app)
            .post('/api/collection/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: "C1",
                group: "62a642f011e66abb6b9c0391"
            })
            .expect(404)

    })

})



describe("Testing fetch a collection", () => {

    it('returns a 200 on successful fetch', async () => {
        const c = await request(app)
            .post('/api/collection/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'C1',
                group: global.mocksObjects.group
            })
            .expect(201)

        await request(app)
            .get(`/api/collection/${c.body.collection._id}`)
            .set({ Authorization: global.gmAccessToken })
            .expect(200)

    })

    it('returns a 422 with code 413 for wrong id format', async () => {

        await request(app)
            .get(`/api/collection/123`)
            .set({ Authorization: global.gmAccessToken })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(413))

    })


    it('returns a 404 with if resource not found', async () => {

        await request(app)
            .get(`/api/collection/62a642f011e66abb6b9c0391`)
            .set({ Authorization: global.gmAccessToken })
            .expect(404)

    })


})