import express from "express";
import { attachUser } from "../../middlewares/attachUser";
import { canPerform } from "../../middlewares/canPerformAction";
import { eliminateRolesScope } from "../../middlewares/groupScope";
import { performValidation } from "../../middlewares/validation";
import { ACTIONS, RESOURCES } from "../../types";
import { createNewCollection, deleteExCollection, getCollections, updateExCollection } from "./collection.controller";
import { createCollectionValidators, getCollectionValidators, updateCollectionValidators } from "./collection.validators";



const collectionRouter = express.Router()


/**
 * @swagger
 * /api/collection/:
 *  post:
 *      tags:
 *          - Collection
 *      description:  Create New Collection
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: C1
 *                                  required: true
 *                              group:
 *                                  type: ObjectID
 *                                  required: true
 *                                  example: 62a642f011e66abb6b9c0391
 *            
 *      responses:
 *          201:
 *              description:  Collection Created
 *                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          411: no name provided
 *                          412: wrong group
 *  
 *   
 */
collectionRouter.post("/", createCollectionValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.COLLECTION), canPerform(ACTIONS.CREATE, RESOURCES.COLLECTION), createNewCollection)



/**
 * @swagger
 * /api/collection/list:
 *  get:
 *      tags:
 *          - Collection
 *      description:  List all collections
 *            
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                                  collections:
 *                                      type: array                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *  
 *   
 */
collectionRouter.get("/list", attachUser, eliminateRolesScope(RESOURCES.COLLECTION), canPerform(ACTIONS.READ_ALL, RESOURCES.COLLECTION), getCollections)


/**
 * @swagger
 * /api/collection/{id}:
 *  get:
 *      tags:
 *          - Collection
 *      description:  Get collection by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          413: wrong id
 *  
 *   
 */
collectionRouter.get("/:id", getCollectionValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.COLLECTION), canPerform(ACTIONS.READ_ONE, RESOURCES.COLLECTION), getCollections)


/**
 * @swagger
 * /api/collection/{id}:
 *  put:
 *      tags:
 *          - Collection
 *      description:  Update collection by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: C1
 *                              group:
 *                                  type: ObjectID
 *                                  example: 62a642f011e66abb6b9c0391
 *              
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          412: wrong group
 *                          413: wrong id  
 *   
 */
collectionRouter.put("/:id", updateCollectionValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.COLLECTION), canPerform(ACTIONS.UPDATE, RESOURCES.COLLECTION), updateExCollection)



/**
 * @swagger
 * /api/collection/{id}:
 *  delete:
 *      tags:
 *          - Collection
 *      description:  Delete collection by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *  
 *   
 */
collectionRouter.delete("/:id", getCollectionValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.COLLECTION), canPerform(ACTIONS.DELETE, RESOURCES.COLLECTION), deleteExCollection)

export {
    collectionRouter
}