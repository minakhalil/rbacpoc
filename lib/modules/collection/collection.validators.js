import { body, param } from 'express-validator'

export const createCollectionValidators =
    [
        body('name').notEmpty().withMessage(["no name provided", 411]),
        body('group').notEmpty().withMessage(["missing or invalid group id", 412]),
        body('group').custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid group id", 412])
    ]


export const getCollectionValidators =
    [
        param('id').exists().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid collection id", 413])
    ]


export const updateCollectionValidators =
    [
        ...getCollectionValidators,
        body('group').optional().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid group id", 412])
    ]
