import { CustomError } from "../../utils/customError"
import { getGroupById } from "../group/group.service"
import { deleteItemsByCollectionId } from "../item/item.service"
import { createCollection, getAllCollections, getCollectionById, updateCollection, deleteCollection } from "./collection.service"

export const createNewCollection = async (req, res, next) => {
    try {
        const { name, group } = req.body
        const groupElement = await getGroupById(group)

        if (!groupElement)
            throw new CustomError("No Group Found", 420, 404)

        const collection = await createCollection(name, group)

        res.status(201).json({ added: true, collection })

    } catch (e) {
        next(e)
    }
}


export const getCollections = async (req, res, next) => {
    try {

        const { id } = req.params

        if (id) {
            const collection = await getCollectionById(id)

            if (!collection)
                throw new CustomError("Collection not found", 421, 404)

            return res.status(200).json({ collection })
        }
        const { scopedGroups } = req.user

        const collections = await getAllCollections(scopedGroups)

        res.status(200).json({ collections })

    } catch (e) {
        next(e)
    }
}


export const updateExCollection = async (req, res, next) => {
    try {

        const { id } = req.params
        const { group } = req.body

        const collection = await getCollectionById(id)

        if (!collection)
            throw new CustomError("Collection not found", 421, 404)

        if (group) {
            const groupElement = await getGroupById(group)

            if (!groupElement)
                throw new CustomError("No Group Found", 420, 404)
        }

        await updateCollection(id, req.body)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}

export const deleteExCollection = async (req, res, next) => {
    try {

        const { id } = req.params

        const collection = await getCollectionById(id)

        if (!collection)
            throw new CustomError("Collection not found", 421, 404)

        await Promise.all([
            deleteCollection(id),
            deleteItemsByCollectionId(id)
        ])

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}