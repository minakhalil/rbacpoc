import mongoose from 'mongoose'

const itemSchema = new mongoose.Schema({

    name: {
        type: String,
        trim: true
    },
    _collection: {
        type: mongoose.Schema.ObjectId,
        ref: 'Collection'
    },
    group: {
        type: mongoose.Schema.ObjectId,
        ref: 'Group'
    }

}, {
    timestamps: true
})

export default mongoose.model('Item', itemSchema)