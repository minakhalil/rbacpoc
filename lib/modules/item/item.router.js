import express from "express";
import { attachUser } from "../../middlewares/attachUser";
import { canPerform } from "../../middlewares/canPerformAction";
import { eliminateRolesScope } from "../../middlewares/groupScope";
import { performValidation } from "../../middlewares/validation";
import { ACTIONS, RESOURCES } from "../../types";
import { createNewItem, deleteExItem, getItems, updateExItem } from "./item.controller";
import { createItemValidators, getItemValidators, updateItemValidators } from "./item.validators";



const itemRouter = express.Router()



/**
 * @swagger
 * /api/item/:
 *  post:
 *      tags:
 *          - Item
 *      description:  Create New Item
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: I1
 *                                  required: true
 *                              collection:
 *                                  type: ObjectID
 *                                  required: true
 *                                  example: 62a642f011e66abb6b9c0391
 *            
 *      responses:
 *          201:
 *              description:  Item Created
 *                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          511: no name provided
 *                          512: wrong collection
 *  
 *   
 */
itemRouter.post("/", createItemValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.ITEM), canPerform(ACTIONS.CREATE, RESOURCES.ITEM), createNewItem)


/**
 * @swagger
 * /api/item/list:
 *  get:
 *      tags:
 *          - Item
 *      description:  List all item
 *            
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                                  items:
 *                                      type: array                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *  
 *   
 */
itemRouter.get("/list", attachUser, eliminateRolesScope(RESOURCES.ITEM), canPerform(ACTIONS.READ_ALL, RESOURCES.ITEM), getItems)



/**
 * @swagger
 * /api/item/{id}:
 *  get:
 *      tags:
 *          - Item
 *      description:  Get item by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          513: wrong id
 *  
 *   
 */
itemRouter.get("/:id", getItemValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.ITEM), canPerform(ACTIONS.READ_ONE, RESOURCES.ITEM), getItems)



/**
 * @swagger
 * /api/item/{id}:
 *  put:
 *      tags:
 *          - Item
 *      description:  Update item by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: I1
 *                              collection:
 *                                  type: ObjectID
 *                                  example: 62a642f011e66abb6b9c0391
 *              
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          512: wrong collection
 *                          513: wrong id  
 *   
 */
itemRouter.put("/:id", updateItemValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.ITEM), canPerform(ACTIONS.UPDATE, RESOURCES.ITEM), updateExItem)






/**
 * @swagger
 * /api/item/{id}:
 *  delete:
 *      tags:
 *          - Item
 *      description:  Delete item by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *  
 *   
 */
itemRouter.delete("/:id", getItemValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.ITEM), canPerform(ACTIONS.DELETE, RESOURCES.ITEM), deleteExItem)

export {
    itemRouter
}