import Item from './item.model'

export const createItem = async (name, cid, gid) => {
    return await Item.create({
        name,
        _collection: cid,
        group: gid
    })
}

export const getItemById = async (id) => {
    return await Item.findById(id)
}

export const getAllItems = async (_scope) => {
    if (_scope === undefined)
        return await Item
            .find()
            .populate('group')
            .populate('_collection')
            .exec()

    return await Item
        .find({
            group: _scope
        })
        .populate('group')
        .populate('_collection')
        .exec()
}

export const updateItem = async (id, params) => {
    return await Item.findOneAndUpdate({ _id: id }, {
        ...params
    })
}

export const deleteItem = async (id) => {
    return await Item.findByIdAndDelete(id)
}

export const deleteItemsByGroupId = async (id) => {
    return await Item.deleteMany({ group: id })
}


export const deleteItemsByCollectionId = async (id) => {
    return await Item.deleteMany({ _collection: id })
}


