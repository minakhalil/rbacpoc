import { body, param } from 'express-validator'

export const createItemValidators =
    [
        body('name').notEmpty().withMessage(["no name provided", 511]),
        body('collection').notEmpty().withMessage(["missing or invalid collection id", 512]),
        body('collection').custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid collection id", 512]),
    ]


export const getItemValidators =
    [
        param('id').exists().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid item id", 513])
    ]


export const updateItemValidators =
    [
        ...getItemValidators,
        body('collection').optional().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid collection id", 512])
    ]
