import request from 'supertest'
import { app } from '../../../server/app'


describe("Testing Add new item", () => {

    it('returns a 201 on successful item creation', async () => {

        await request(app)
            .post('/api/item/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'I1',
                collection: global.mocksObjects.collection
            })
            .expect(201)
    })


    it('returns a 422 with codes 511 with an missing name', async () => {

        await request(app)
            .post('/api/item/')
            .set({ Authorization: global.gmAccessToken })
            .send({

            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(511))

    })


    it('returns a 422 with codes 512 with an missing collection', async () => {

        await request(app)
            .post('/api/item/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: "I1"
            })
            .expect(422)
            .expect(res => {
                expect(res.body.code).toEqual(512)
            })

    })

    it('returns a 404 for non exisiting collection', async () => {

        await request(app)
            .post('/api/item/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: "I1",
                collection:"62a642f011e66abb6b9c0391"
            })
            .expect(404)

    })

})



describe("Testing fetch a item", () => {

    it('returns a 200 on successful fetch', async () => {
        const i = await request(app)
            .post('/api/item/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'I1',
                collection: global.mocksObjects.collection
            })
            .expect(201)

        await request(app)
            .get(`/api/item/${i.body.item._id}`)
            .set({ Authorization: global.gmAccessToken })
            .expect(200)

    })

    it('returns a 422 with code 513 for wrong id format', async () => {

        await request(app)
            .get(`/api/item/123`)
            .set({ Authorization: global.gmAccessToken })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(513))

    })


    it('returns a 404 with if resource not found', async () => {

        await request(app)
            .get(`/api/item/62a642f011e66abb6b9c0391`)
            .set({ Authorization: global.gmAccessToken })
            .expect(404)

    })


})