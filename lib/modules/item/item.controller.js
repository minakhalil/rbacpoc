import { CustomError } from "../../utils/customError"
import { getCollectionById } from "../collection/collection.service"
import { createItem, deleteItem, getAllItems, getItemById, updateItem } from "./item.service"

export const createNewItem = async (req, res, next) => {
    try {
        const { name, collection } = req.body

        const collectionElement = await getCollectionById(collection)

        if (!collectionElement)
            throw new CustomError("Collection not found", 520, 404)

        const item = await createItem(name, collection, collectionElement.group)

        res.status(201).json({ added: true, item })

    } catch (e) {
        next(e)
    }
}


export const getItems = async (req, res, next) => {
    try {

        const { id } = req.params

        if (id) {
            const item = await getItemById(id)

            if (!item)
                throw new CustomError("Item not found", 521, 404)

            return res.status(200).json({ item })
        }
        const { scopedGroups } = req.user

        const items = await getAllItems(scopedGroups)

        res.status(200).json({ items })

    } catch (e) {
        next(e)
    }
}


export const updateExItem = async (req, res, next) => {
    try {

        const { id } = req.params
        const { collection } = req.body


        const item = await getItemById(id)

        if (!item)
            throw new CustomError("Item not found", 521, 404)

        if (collection) {
            const collectionElement = await getCollectionById(collection)

            if (!collectionElement)
                throw new CustomError("Collection not found", 520, 404)
        }

        await updateItem(id, req.body)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}

export const deleteExItem = async (req, res, next) => {
    try {

        const { id } = req.params

        const item = await getItemById(id)

        if (!item)
            throw new CustomError("Item not found", 521, 404)

        await deleteItem(id)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}