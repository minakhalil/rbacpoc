import express from "express";
import { attachUser } from "../../middlewares/attachUser";
import { canPerform } from "../../middlewares/canPerformAction";
import { eliminateRolesScope } from "../../middlewares/groupScope";
import { performValidation } from "../../middlewares/validation";
import { ACTIONS, RESOURCES } from "../../types";
import { createNewGroup, deleteExGroup, getGroups, updateExGroup } from "./group.controller";
import { createGroupValidators, getGroupValidators, updateGroupValidators } from "./group.validators";



const groupRouter = express.Router()


/**
 * @swagger
 * /api/group/:
 *  post:
 *      tags:
 *          - Group
 *      description:  Create New Group
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: G1
 *                                  required: true
 *            
 *      responses:
 *          201:
 *              description:  Group Created
 *                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          311: no name provided
 *  
 *   
 */
groupRouter.post("/", createGroupValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.GROUP), canPerform(ACTIONS.CREATE, RESOURCES.GROUP), createNewGroup)


/**
 * @swagger
 * /api/group/list:
 *  get:
 *      tags:
 *          - Group
 *      description:  List all groups
 *            
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                                  groups:
 *                                      type: array                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *  
 *   
 */
groupRouter.get("/list", attachUser, eliminateRolesScope(RESOURCES.GROUP), canPerform(ACTIONS.READ_ALL, RESOURCES.GROUP), getGroups)




/**
 * @swagger
 * /api/group/{id}:
 *  get:
 *      tags:
 *          - Group
 *      description:  Get group by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          312: wrong id
 *  
 *   
 */
groupRouter.get("/:id", getGroupValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.GROUP), canPerform(ACTIONS.READ_ONE, RESOURCES.GROUP), getGroups)






/**
 * @swagger
 * /api/group/{id}:
 *  put:
 *      tags:
 *          - Group
 *      description:  Update group by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              name:
 *                                  type: string
 *                                  example: G1
 *              
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          512: wrong id  
 *   
 */
groupRouter.put("/:id", updateGroupValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.GROUP), canPerform(ACTIONS.UPDATE, RESOURCES.GROUP), updateExGroup)



/**
 * @swagger
 * /api/group/{id}:
 *  delete:
 *      tags:
 *          - Group
 *      description:  Delete group by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *  
 *   
 */
groupRouter.delete("/:id", getGroupValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.GROUP), canPerform(ACTIONS.DELETE, RESOURCES.GROUP), deleteExGroup)

export {
    groupRouter
}