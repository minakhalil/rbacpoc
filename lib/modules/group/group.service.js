import Group from './group.model'

export const createGroup = async (name) => {
    return await Group.create({
        name
    })
}

export const getGroupById = async (id) => {
    return await Group.findById(id)
}

export const getAllGroups = async (_scope) => {
    if (_scope === undefined)
        return await Group.find()

    return await Group.find({
        _id: _scope
    })
}

export const updateGroup = async (id, params) => {
    return await Group.findOneAndUpdate({ _id: id }, {
        ...params
    })
}

export const deleteGroup = async (id) => {
    return await Group.findByIdAndDelete(id)
}


