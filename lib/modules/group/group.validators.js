import { body, param } from 'express-validator'

export const createGroupValidators =
    [
        body('name').notEmpty().withMessage(["no name provided", 311]),
    ]


export const getGroupValidators =
    [
        param('id').exists().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid group id", 312])
    ]


export const updateGroupValidators =
    [
        ...getGroupValidators,
    ]
