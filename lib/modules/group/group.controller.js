import { CustomError } from "../../utils/customError"
import { deleteCollectionsByGroupId } from "../collection/collection.service"
import { deleteItemsByGroupId } from "../item/item.service"
import { createGroup, deleteGroup, getAllGroups, getGroupById, updateGroup } from "./group.service"

export const createNewGroup = async (req, res, next) => {
    try {
        const { name } = req.body

        const group = await createGroup(name)

        res.status(201).json({ added: true, group })

    } catch (e) {
        next(e)
    }
}


export const getGroups = async (req, res, next) => {
    try {

        const { id } = req.params

        if (id) {
            const group = await getGroupById(id)

            if (!group)
                throw new CustomError("Group not found", 320, 404)

            return res.status(200).json({ group })
        }
        const { scopedGroups } = req.user

        const groups = await getAllGroups(scopedGroups)

        res.status(200).json({ groups })

    } catch (e) {
        next(e)
    }
}


export const updateExGroup = async (req, res, next) => {
    try {

        const { id } = req.params

        const group = await getGroupById(id)

        if (!group)
            throw new CustomError("Group not found", 320, 404)


        await updateGroup(id, req.body)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}

export const deleteExGroup = async (req, res, next) => {
    try {

        const { id } = req.params

        const group = await getGroupById(id)

        if (!group)
            throw new CustomError("Group not found", 320, 404)


        await Promise.all([
            deleteGroup(id),
            deleteCollectionsByGroupId(id),
            deleteItemsByGroupId(id)
        ])

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}