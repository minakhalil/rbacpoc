import request from 'supertest'
import { app } from '../../../server/app'


describe("Testing Add new group", () => {

    it('returns a 201 on successful group creation', async () => {
        await request(app)
            .post('/api/group/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'G1',
            })
            .expect(201)
    })


    it('returns a 422 with codes 311 with an missing name', async () => {

        await request(app)
            .post('/api/group/')
            .set({ Authorization: global.gmAccessToken })
            .send({

            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(311))

    })

})



describe("Testing fetch a group", () => {

    it('returns a 200 on successful fetch', async () => {
        const g = await request(app)
            .post('/api/group/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                name: 'G1',
            })
            .expect(201)

        await request(app)
            .get(`/api/group/${g.body.group._id}`)
            .set({ Authorization: global.gmAccessToken })
            .expect(200)

    })

    it('returns a 422 with code 312 for wrong id format', async () => {

        await request(app)
            .get(`/api/group/123`)
            .set({ Authorization: global.gmAccessToken })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(312))

    })


    it('returns a 404 with if resource not found', async () => {

        await request(app)
            .get(`/api/group/62a642f011e66abb6b9c0391`)
            .set({ Authorization: global.gmAccessToken })
            .expect(404)

    })


})