import { CustomError } from "../../utils/customError"
import { generateAccessToken } from "../../utils/token"
import { getUserByEmail } from "../user/user.service"

export const login = async (req, res, next) => {
    try {
        const { email } = req.body

        const user = await getUserByEmail(email)

        if (!user)
            throw new CustomError("Not a user", 113, 401)

        //generate JWT access token
        const payload = {
            uid: user._id.toString()
        }
        const accessToken = generateAccessToken(payload)
        
        res.status(200).json({ accessToken })

    } catch (e) {
        next(e)
    }
}


