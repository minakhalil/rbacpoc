

import express from "express";
import { performValidation } from "../../middlewares/validation";
import { login } from "./auth.controller";
import { loginValidators } from "./auth.validators";


const authRouter = express.Router()

/**
 * @swagger
 * /api/auth/login:
 *  post:
 *      tags:
 *          - Authentication
 *      description:  Authenticate user
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                                  example: i@ignore.com
 *            
 *      responses:
 *          200:
 *              description:  Successful
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              accessToken:
 *                                  type: string
 *                                  
 *          401:
 *              description:  Not a user
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          111: no email provided
 *                          112: wrong email format
 *  
 *   
 */
authRouter.post("/login", loginValidators, performValidation, login)





export {
    authRouter
}
