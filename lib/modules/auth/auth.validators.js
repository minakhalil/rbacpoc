import { body } from 'express-validator'

export const loginValidators =
    [
        body('email').notEmpty().withMessage(["no email provided", 111]),
        body('email').normalizeEmail().isEmail().withMessage(["please re-check your email", 112])
    ]