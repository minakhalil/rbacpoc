import request from 'supertest'
import { app } from '../../../server/app'
import { USER_TYPES } from '../../types'

describe("Testing Login", () => {

    it('fails when a email that does not exist is supplied', async () => {
        await request(app)
            .post('/api/auth/login')
            .send({
                email: 'test@test.com'
            })
            .expect(401)
    })



    it('responds with a access token  when given valid credentials', async () => {
        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test@test.com',
                newRoles: {
                    [USER_TYPES.REGULAR]: ['62a64a956065ac5790c44cb0']
                }
            })

        await request(app)
            .post('/api/auth/login')
            .send({
                email: 'test@test.com'
            })
            .expect(200)
            .expect((res) => {
                expect(res.body.accessToken).toBeDefined()
            })
    })

})