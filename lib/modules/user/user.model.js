import mongoose from 'mongoose'
import { USER_TYPES } from '../../types'

const userSchema = new mongoose.Schema({

    email: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    roles: {
        [USER_TYPES.REGULAR]: [{
            type: mongoose.Schema.ObjectId,
            ref: 'Group'
        }],
        [USER_TYPES.GROUP_MANAGER]: [{
            type: mongoose.Schema.ObjectId,
            ref: 'Group'
        }],
        [USER_TYPES.GENERAL_MANAGER]: {
            type: Boolean,
            default: false
        }
    }

}, {
    timestamps: true
})

export default mongoose.model('User', userSchema)