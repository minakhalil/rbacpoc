import { USER_TYPES } from '../../types'
import User from './user.model'


export const createUser = async (email, roles) => {
    return await User.create({
        email, roles
    })
}

export const getUserByEmail = async (email) => {
    return await User.findOne({ email })
}

export const getUserById = async (id) => {
    return await User.findById(id)
}

export const getAllUsers = async (_scope) => {
    if (_scope === undefined)
        return await User
            .find()
            .populate({
                path: "roles",
                populate: [{
                    path: `${USER_TYPES.GROUP_MANAGER}`,
                    model: "Group"
                }, {
                    path: `${USER_TYPES.REGULAR}`,
                    model: "Group"
                }]
            })

    return await User
        .find({
            $or: [
                {
                    [`roles.${USER_TYPES.REGULAR}`]: { $in: _scope }

                },
                {
                    [`roles.${USER_TYPES.GROUP_MANAGER}`]: { $in: _scope }

                }

            ]
        })
        .populate({
            path: "roles",
            populate: [{
                path: `${USER_TYPES.GROUP_MANAGER}`,
                model: "Group"
            }, {
                path: `${USER_TYPES.REGULAR}`,
                model: "Group"
            }]
        })
}

export const updateUser = async (id, email, newRoles = {}, revokedRoles = {}) => {
    await User.findOneAndUpdate({ _id: id }, {
        email,
        $addToSet: {
            [`roles.${USER_TYPES.GROUP_MANAGER}`]: {
                $each: newRoles[USER_TYPES.GROUP_MANAGER] || []
            },
            [`roles.${USER_TYPES.REGULAR}`]: {
                $each: newRoles[USER_TYPES.REGULAR] || []
            }
        }
    })

    return await User.findOneAndUpdate({ _id: id }, {
        $pull: {
            [`roles.${USER_TYPES.GROUP_MANAGER}`]: {
                $in: revokedRoles[USER_TYPES.GROUP_MANAGER] || []
            },
            [`roles.${USER_TYPES.REGULAR}`]: {
                $in: revokedRoles[USER_TYPES.REGULAR] || []
            }
        }
    })
}


export const deleteUser = async (id) => {
    return await User.findByIdAndDelete(id)
}


export const deleteUsersOfGroupId = async (id) => {
    return await User.updateMany({
        $pull: {
            [`roles.${USER_TYPES.GROUP_MANAGER}`]: id,
            [`roles.${USER_TYPES.REGULAR}`]: id
        }
    })
}




