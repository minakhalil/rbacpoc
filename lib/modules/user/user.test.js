import request from 'supertest'
import { app } from '../../../server/app'
import { USER_TYPES } from '../../types'


describe("Testing Add new user", () => {

    it('returns a 201 on successful user creation', async () => {
        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test@test.com',
                newRoles: {
                    [USER_TYPES.REGULAR]: ['62a64a956065ac5790c44cb0']
                }
            })
            .expect(201)
    })

    it('returns a code 212 with an invalid email', async () => {
        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test',
            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(212))
    })

    it('returns a code 213 with an invalid newRoles Object', async () => {

        await Promise.all([
            request(app)
                .post('/api/user/')
                .set({ Authorization: global.gmAccessToken })
                .send({
                    email: 'test@tets.com',
                    newRoles: null
                })
                .expect(422)
                .expect(res => expect(res.body.code).toEqual(213)),

            request(app)
                .post('/api/user/')
                .set({ Authorization: global.gmAccessToken })
                .send({
                    email: 'test@tets.com',
                    newRoles: {
                        [USER_TYPES.REGULAR]: {}
                    }
                })
                .expect(422)
                .expect(res => expect(res.body.code).toEqual(213)),

            request(app)
                .post('/api/user/')
                .set({ Authorization: global.gmAccessToken })
                .send({
                    email: 'test@tets.com',
                    newRoles: {
                        [USER_TYPES.REGULAR]: ["123"]
                    }
                })
                .expect(422)
                .expect(res => expect(res.body.code).toEqual(213))
        ])
    })

    it('disallows duplicate emails and return code 220', async () => {
        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test@test.com',
                newRoles: {
                    [USER_TYPES.REGULAR]: ['62a64a956065ac5790c44cb0']
                }
            })
            .expect(201)

        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test@test.com',
                newRoles: {
                    [USER_TYPES.REGULAR]: ['62a64a956065ac5790c44cb0']
                }
            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(220))
    })


    it('returns a 422 with codes 211 with an missing email', async () => {

        await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({

            })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(211))

    })

})



describe("Testing fetch a user", () => {

    it('returns a 200 on successful fetch', async () => {
        const u = await request(app)
            .post('/api/user/')
            .set({ Authorization: global.gmAccessToken })
            .send({
                email: 'test@test.com',
                newRoles: {
                    [USER_TYPES.REGULAR]: ['62a64a956065ac5790c44cb0']
                }
            })
            .expect(201)

        await request(app)
            .get(`/api/user/${u.body.user._id}`)
            .set({ Authorization: global.gmAccessToken })
            .expect(200)

    })

    it('returns a 422 with code 214 for wrong id format', async () => {

        await request(app)
            .get(`/api/user/123`)
            .set({ Authorization: global.gmAccessToken })
            .expect(422)
            .expect(res => expect(res.body.code).toEqual(214))

    })


    it('returns a 404 with if resource not found', async () => {

        await request(app)
            .get(`/api/user/62a642f011e66abb6b9c0391`)
            .set({ Authorization: global.gmAccessToken })
            .expect(404)

    })

})