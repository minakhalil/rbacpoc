import { body, param } from 'express-validator'
import { USER_TYPES } from '../../types'

const _checkRolesObjectStructure = (roles) => {

    if (
        (roles instanceof Object) &&
        (roles[USER_TYPES.GROUP_MANAGER] !== undefined || roles[USER_TYPES.REGULAR] !== undefined) &&
        (roles[USER_TYPES.GROUP_MANAGER] === undefined || (Array.isArray(roles[USER_TYPES.GROUP_MANAGER]) && roles[USER_TYPES.GROUP_MANAGER].every(r => r.match(/^[0-9a-fA-F]{24}$/) !== null))) &&
        (roles[USER_TYPES.REGULAR] === undefined || (Array.isArray(roles[USER_TYPES.REGULAR]) && roles[USER_TYPES.REGULAR].every(r => r.match(/^[0-9a-fA-F]{24}$/) !== null)))
    )
        return true

    return false
}
export const createUserValidators =
    [
        body('email').notEmpty().withMessage(["no email provided", 211]),
        body('email').normalizeEmail().isEmail().withMessage(["please re-check email", 212]),

        body('newRoles').notEmpty().withMessage(["missing or invalid roles object", 213]),
        body('newRoles').custom(roles => _checkRolesObjectStructure(roles)).withMessage(["missing or invalid roles object", 213])
    ]


export const getUserValidators =
    [
        param('id').exists().custom(id => id.match(/^[0-9a-fA-F]{24}$/)).withMessage(["missing or invalid user id", 214])
    ]


export const updateUserValidators =
    [
        ...getUserValidators,
        body('email').optional().normalizeEmail().isEmail().withMessage(["please re-check email", 212]),

        body('newRoles').optional().custom(roles => _checkRolesObjectStructure(roles)).withMessage(["missing or invalid roles object", 213]),
        body('revokedRoles').optional().custom(roles => _checkRolesObjectStructure(roles)).withMessage(["missing or invalid roles object", 213])
    ]
