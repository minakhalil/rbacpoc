import express from "express";
import { attachUser } from "../../middlewares/attachUser";
import { canPerform } from "../../middlewares/canPerformAction";
import { eliminateRolesScope } from "../../middlewares/groupScope";
import { performValidation } from "../../middlewares/validation";
import { ACTIONS, RESOURCES } from "../../types";
import { createNewUser, deleteExUser, getUsers, updateExUser } from "./user.controller";
import { createUserValidators, getUserValidators, updateUserValidators } from "./user.validators";



const userRouter = express.Router()


/**
 * @swagger
 * /api/user/:
 *  post:
 *      tags:
 *          - User
 *      description:  Create New User
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                                  example: U1
 *                                  required: true
 *                              newRoles:
 *                                  type: object
 *                                  required: true
 *                                  properties:
 *                                      regular:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *                                      group_manager:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *            
 *      responses:
 *          201:
 *              description:  User Created
 *                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          211: no email provided
 *                          212: recheck email
 *                          213: wrong newRoles format
 *                          220: duplicate email
 *  
 *   
 */
userRouter.post("/", createUserValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.USER), canPerform(ACTIONS.CREATE, RESOURCES.USER), createNewUser)


/**
 * @swagger
 * /api/user/list:
 *  get:
 *      tags:
 *          - User
 *      description:  List all users
 *            
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                                  groups:
 *                                      type: array                                  
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *  
 *   
 */
userRouter.get("/list", attachUser, eliminateRolesScope(RESOURCES.USER), canPerform(ACTIONS.READ_ALL, RESOURCES.USER), getUsers)



/**
 * @swagger
 * /api/user/{id}:
 *  get:
 *      tags:
 *          - User
 *      description:  Get user by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          200:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          214: wrong id
 *  
 *   
 */
userRouter.get("/:id", getUserValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.USER), canPerform(ACTIONS.READ_ONE, RESOURCES.USER), getUsers)








/**
 * @swagger
 * /api/user/{id}:
 *  put:
 *      tags:
 *          - User
 *      description:  Update user by id, if email change happend will send to both old/new
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *      requestBody:
 *            required: true
 *            content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                                  example: U1
 *                                  required: true
 *                              newRoles:
 *                                  type: object
 *                                  required: true
 *                                  properties:
 *                                      regular:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *                                      group_manager:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *                              revokedRoles:
 *                                  type: object
 *                                  required: true
 *                                  description: dfjfsdjkb
 *                                  properties:
 *                                      regular:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *                                      group_manager:
 *                                          type: array
 *                                          items:
 *                                              type: ObjectID
 *              
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *          422:
 *              description:  Error with API __Need to check code to determine error__
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              code:
 *                                  type: number
 *                              msg:
 *                                  type: string
 *                      example:
 *                          214: wrong id  
 *                          212: recheck email  
 *                          213: wrong newRoles/revokedRoles format   
 *   
 */
userRouter.put("/:id", updateUserValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.USER), canPerform(ACTIONS.UPDATE, RESOURCES.USER), updateExUser)




/**
 * @swagger
 * /api/user/{id}:
 *  delete:
 *      tags:
 *          - User
 *      description:  Delete user by id
 *      parameters:
 *          - name: id
 *            in: path
 *            required: true
 *            type: ObjectID
 *            example: 62a642f011e66abb6b9c0391
 *             
 *      responses:
 *          204:
 *              description:  Successfull
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object                                 
 *          401:
 *              description:  Not Authenticated
 *          403:
 *              description:  Not Authorized
 *          404:
 *              description:  Not Found
 *  
 *   
 */
userRouter.delete("/:id", getUserValidators, performValidation, attachUser, eliminateRolesScope(RESOURCES.USER), canPerform(ACTIONS.DELETE, RESOURCES.USER), deleteExUser)

export {
    userRouter
}