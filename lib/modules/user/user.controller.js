import { send } from "../../services/email"
import { CustomError } from "../../utils/customError"
import { EmailChangeTemplate } from "../../utils/templates"
import { createUser, deleteUser, getAllUsers, getUserByEmail, getUserById, updateUser } from "./user.service"

export const createNewUser = async (req, res, next) => {
    try {
        const { email, newRoles } = req.body

        const userWithSameEmail = await getUserByEmail(email)

        if (userWithSameEmail)
            throw new CustomError("This email has been used before", 220)

        const user = await createUser(email, newRoles)

        res.status(201).json({ added: true, user })

    } catch (e) {
        next(e)
    }
}


export const getUsers = async (req, res, next) => {
    try {

        const { id } = req.params

        if (id) {
            const user = await getUserById(id)

            if (!user)
                throw new CustomError("User not found", 221, 404)

            return res.status(200).json({ user })
        }
        const { scopedGroups } = req.user

        const users = await getAllUsers(scopedGroups)

        res.status(200).json({ users })

    } catch (e) {
        next(e)
    }
}


export const updateExUser = async (req, res, next) => {
    try {

        const { id } = req.params
        const { email, newRoles, revokedRoles } = req.body


        const user = await getUserById(id)
        if (!user)
            throw new CustomError("User not found", 221, 404)

        const oldEmail = user.email

        //async send emails to both old,new in case user submit a diff email
        if (email) {

            if (email !== oldEmail) {
                const { subject, body } = EmailChangeTemplate(oldEmail, email)

                send(oldEmail, subject, body)
                send(user.email, subject, body)
            }
        }

        await updateUser(id, email || oldEmail, newRoles, revokedRoles)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}

export const deleteExUser = async (req, res, next) => {
    try {

        const { id } = req.params

        const user = await getItemById(id)

        if (!user)
            throw new CustomError("User not found", 221, 404)

        await deleteUser(id)

        res.status(204).json({})

    } catch (e) {
        next(e)
    }
}