import nodemailer from 'nodemailer'
import { EmailConfig } from '../config'
import { logger } from '../utils/logger'


let transporter = nodemailer.createTransport({
    host: EmailConfig.host,
    port: EmailConfig.port,
    secure: EmailConfig.secure,
    auth: EmailConfig.username ? {
        user: EmailConfig.username,
        pass: EmailConfig.password
    } : undefined,
    tls: {
        rejectUnauthorized: false
    }
})

/**
 * Sending email using nodemailer
 * @param {string} mail recepient email address
 * @param {string} subject email subject
 * @param {string} text email body
 */
export const send = async (mail, subject, text) => {
    try {
        logger.info(`Sending email to ${mail}`)
        await transporter.sendMail({
            from: EmailConfig.from,
            to: mail,
            subject,
            text
        })
        logger.info("Email sent successfully")
    } catch (e) {
        logger.error("Can't send email, Problem with SMTP server")
        logger.error(e)
    }
}