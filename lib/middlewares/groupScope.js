import { rbacMapping } from "../config/rbac"
import { getCollectionById } from "../modules/collection/collection.service"
import { getItemById } from "../modules/item/item.service"
import { getUserById } from "../modules/user/user.service"
import { ACTIONS, RESOURCES, USER_TYPES } from "../types"
import { CustomError } from "../utils/customError"


/**
 * 
 * 
 * [PHASE-1] of the Authorization process
 *  [-] Used to filter the roles that user need to access this resource
 *  [-] Scope the group access to only groups he owns
 *  
 * 
 * 
 * @param {*} resource User,Group,Collection,Item
 * @returns Callback next(), attach scopedGroups to req.user
 * @throws NotAuthorized
 */
export const eliminateRolesScope = (resource) => {
    return async (req, res, next) => {

        const { id } = req.params

        try {
            /**
             * GENERAL_MANAGER
             * -> skip
             */
            if (req.user.roles[USER_TYPES.GENERAL_MANAGER]) {
                return next()
            }


            /**
             * Resource Based filtering/elimination
             * 
             * [-] Effective in actions READ_ONE,CREATE,UPDATE,DELETE
             * [-] Filter user roles by only the related grooup to the addressed resource
             * [-] Check for access resources in params/body [DEEP_ACCESS]
             * 
             * Example for DEEP_ACCESS
             *  - User u1 admin for group g1
             *  - User u2 regular in group g1
             *  - User u1 have full access to edit the roles of u2
             *  - User u1 try to edit u2 to attach him a regular access for g2
             *  - User u1 not an admin in g2 -> DEEP_ACCESS
             */
            

            switch (resource) {

                case RESOURCES.GROUP:

                    if (id) {
                        req.user.roles[USER_TYPES.GROUP_MANAGER] = req.user.roles[USER_TYPES.GROUP_MANAGER].filter(r => r.toString() == id.toString())
                        req.user.roles[USER_TYPES.REGULAR] = req.user.roles[USER_TYPES.REGULAR].filter(r => r.toString() == id.toString())
                    }
                    break;



                case RESOURCES.USER:

                    if (req.body.newRoles || req.body.revokedRoles) {

                        req.body.newRoles = req.body.newRoles || {}
                        req.body.revokedRoles = req.body.revokedRoles || {}

                        const groups = [
                            ...req.body.newRoles[USER_TYPES.REGULAR] || [],
                            ...req.body.newRoles[USER_TYPES.GROUP_MANAGER] || [],
                            ...req.body.revokedRoles[USER_TYPES.REGULAR] || [],
                            ...req.body.revokedRoles[USER_TYPES.GROUP_MANAGER] || [],
                        ]

                        for (let role of groups)
                            if (!req.user.roles[USER_TYPES.GROUP_MANAGER].map(e => e.toString()).includes(role))
                                throwNotAuthorizedError()

                    }

                    if (id) {
                        let userGroups = await getUserById(id)
                        userGroups = [...userGroups.roles[USER_TYPES.GROUP_MANAGER], ...userGroups.roles[USER_TYPES.REGULAR]].map(e => e.toString())

                        req.user.roles[USER_TYPES.GROUP_MANAGER] = req.user.roles[USER_TYPES.GROUP_MANAGER].filter(r => userGroups.includes(r.toString()))
                        req.user.roles[USER_TYPES.REGULAR] = req.user.roles[USER_TYPES.REGULAR].filter(r => userGroups.includes(r.toString()))
                    }

                    break;



                case RESOURCES.COLLECTION:

                    if (req.body.group) {
                        if (!req.user.roles[USER_TYPES.GROUP_MANAGER].map(e => e.toString()).includes(req.body.group))
                            throwNotAuthorizedError()
                    }

                    if (id) {
                        const { group } = await getCollectionById(id)
                        req.user.roles[USER_TYPES.GROUP_MANAGER] = req.user.roles[USER_TYPES.GROUP_MANAGER].filter(r => r.toString() == group.toString())
                        req.user.roles[USER_TYPES.REGULAR] = req.user.roles[USER_TYPES.REGULAR].filter(r => r.toString() == group.toString())
                    }

                    break;



                case RESOURCES.ITEM:

                    if (req.body.collection) {
                        const { group } = await getCollectionById(req.body.collection)
                        if (!req.user.roles[USER_TYPES.GROUP_MANAGER].map(e => e.toString()).includes(group.toString()))
                            throwNotAuthorizedError()
                    }

                    if (id) {
                        const { group } = await getItemById(id)
                        req.user.roles[USER_TYPES.GROUP_MANAGER] = req.user.roles[USER_TYPES.GROUP_MANAGER].filter(r => r.toString() == group.toString())
                        req.user.roles[USER_TYPES.REGULAR] = req.user.roles[USER_TYPES.REGULAR].filter(r => r.toString() == group.toString())
                    }

                    break;
            }



            /**
             * Attach scopedGroups with the request to be used in READ_ALL
             *  
             *  -> scopedGroups will be undefined in case of GENERAL_MANAGER
             */
             req.user.scopedGroups=[]
             
             if(rbacMapping[USER_TYPES.REGULAR][resource].includes(ACTIONS.READ_ALL)){
                req.user.scopedGroups=[...req.user.scopedGroups,...req.user.roles[USER_TYPES.REGULAR]]
             }
             if(rbacMapping[USER_TYPES.GROUP_MANAGER][resource].includes(ACTIONS.READ_ALL)){
                req.user.scopedGroups=[...req.user.scopedGroups,...req.user.roles[USER_TYPES.GROUP_MANAGER]]
             }

             if(req.user.roles[USER_TYPES.GENERAL_MANAGER]){
                req.user.scopedGroups=undefined
             }

            return next()

        } catch (e) {
            next(e)
        }
    }
}


const throwNotAuthorizedError = () => {
    throw new CustomError("Not Authorized to access this resource", 261, 403)
}
