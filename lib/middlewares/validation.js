import { validationResult } from 'express-validator'

export const performValidation = (req, res, next) => {
    /**
     * Used to handle all caught errors thrown by express validator
     */
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const errorMessage = errors.array()[0]["msg"]
        return res.status(422).json({ "msg": errorMessage[0], "code": errorMessage[1] });
    }
    next()
}