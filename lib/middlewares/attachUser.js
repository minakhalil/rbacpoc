import { getUserById } from "../modules/user/user.service"
import { CustomError } from "../utils/customError"
import { decodeToken } from "../utils/token"

export const attachUser = async (req, res, next) => {
    /**
     * [-] Extract token from Authorization header
     * [-] Decode and get user payload
     * [-] Fetch user from DB and attach it to the req object
     */
    try {
        const token = req.headers['authorization']

        if (!token) {
            throw new CustomError("Need to provide a valid access token", 0, 401)
        }

        try {
            const user = decodeToken(token)

            req.user = await getUserById(user.uid)

            return next()
            
        } catch (e) {
            throw new CustomError("Invalid token!", 0, 401)
        }

    } catch (e) {
        next(e)
    }


}