import { CustomError } from "../utils/customError"
import { logger } from "../utils/logger"

export const globalErrorHandler = (err, req, res, next) => {
    if (err) {
        if (err instanceof CustomError) {
            return res.status(err.status).send({ 'message': err.msg, "code": err.code })
        }

        /**
         * Global Error handling
         */
        
        logger.error(err)
        res.status(500).send({ 'message': "Invalid Request data" })
    } else {
        next()
    }
}