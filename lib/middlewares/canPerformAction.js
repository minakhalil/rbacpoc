import { rbacMapping } from "../config/rbac"
import { USER_TYPES } from "../types"
import { CustomError } from "../utils/customError"

/**
 * 
 * [PHASE-2] of the Authorization process
 *  [-] Checks if the user is allowed to perform specific action to the resource
 * 
 *  ** in PHASE-1 we have eliminated the user roles that only used to grant access for a resource
 *     now we checks if we still have roles after the elimination and the action is still allowed or not
 * 
 * @param {*} action READ_ONE, READ_ALL, CREATE, UPDATE or DELETE
 * @param {*} resource User,Group,Collection or Item
 * @returns next()
 * @throws NotAuthorized
 */
export const canPerform = (action, resource) => {
    return async (req, res, next) => {

        try {
            if (
                (
                    req.user.roles[USER_TYPES.GENERAL_MANAGER]
                ) ||


                (
                    req.user.roles[USER_TYPES.GROUP_MANAGER].length &&
                    rbacMapping[USER_TYPES.GROUP_MANAGER][resource].includes(action)
                ) ||


                (
                    req.user.roles[USER_TYPES.REGULAR].length &&
                    rbacMapping[USER_TYPES.REGULAR][resource].includes(action)
                )
            )

                return next()


            throw new CustomError("Not Authorized to access this resource", 261, 403)

        } catch (e) {
            next(e)
        }
    }
}