export const MainConfig = {
    hostedURL: process.env.HOST_URL || "localhost",
    port: parseInt(process.env.PORT) || 3200,
    serviceName: process.env.SRV_NAME || "rbacPOC",
    logLevel: process.env.LOG_LVL || "debug",
    jwtSecret: process.env.JWT_SECRET || "jwtseceretkey"
}

export const DatabaseConfig = {
    host: process.env.MONGO_SERVER || "127.0.0.1",
    port: parseInt(process.env.MONGO_PORT) || 27017,
    db: process.env.MONGO_DB_NAME || "rbacPOC"
}



export const EmailConfig = {
    host: process.env.SMTP_SERVER || "smtp.ethereal.email",
    secure: process.env.SMTP_SECURE == "true",
    port: parseInt(process.env.SMTP_PORT) || 587,
    username: process.env.SMTP_USERNAME || "rodrick.cremin18@ethereal.email",
    password: process.env.SMTP_PASSWORD || "rQjdF31bZFfVZ76aXY",
    from: process.env.SMTP_FROM_EMAIL || "rodrick.cremin18@ethereal.email"
}

