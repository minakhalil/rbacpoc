import { ACTIONS, RESOURCES, USER_TYPES } from "../types";

export const rbacMapping = {
    /**
     * Association between actions, resources and user types
     */

    [USER_TYPES.REGULAR]: {
        [RESOURCES.USER]: [
            ACTIONS.READ_ONE
        ],
        [RESOURCES.GROUP]: [
            ACTIONS.READ_ONE
        ],
        [RESOURCES.ITEM]: [
            ACTIONS.READ_ONE
        ],
        [RESOURCES.COLLECTION]: [
            ACTIONS.READ_ONE
        ]
    },


    [USER_TYPES.GROUP_MANAGER]: {
        [RESOURCES.USER]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ],
        [RESOURCES.GROUP]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
        ],
        [RESOURCES.ITEM]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ],
        [RESOURCES.COLLECTION]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ]
    },


    [USER_TYPES.GENERAL_MANAGER]: {
        [RESOURCES.USER]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ],
        [RESOURCES.GROUP]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ],
        [RESOURCES.ITEM]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ],
        [RESOURCES.COLLECTION]: [
            ACTIONS.READ_ALL,
            ACTIONS.READ_ONE,
            ACTIONS.CREATE,
            ACTIONS.UPDATE,
            ACTIONS.DELETE
        ]
    },

}