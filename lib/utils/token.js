import jwt from 'jsonwebtoken'
import { MainConfig } from '../config'

export const generateAccessToken = (payload) => jwt.sign(payload, MainConfig.jwtSecret)

export const decodeToken = (token) => jwt.verify(token, MainConfig.jwtSecret)
