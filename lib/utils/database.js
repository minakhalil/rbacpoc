import mongoose from 'mongoose'
import { DatabaseConfig } from '../config'
import { createUser, getUserByEmail } from '../modules/user/user.service'
import { USER_TYPES } from '../types'
import { logger } from './logger'

/**
 * 
 * Database Connection Poller
 * 
 * @returns Promise - only resolved when establish successful connection to database 
 */
export const mongoConnectionInit = () => new Promise((resolve, reject) => {
    var poll = setInterval(async () => {
        try {
            await mongoose.connect(`mongodb://${DatabaseConfig.host}:${DatabaseConfig.port}/${DatabaseConfig.db}`, {
                maxPoolSize: 10,
                ignoreUndefined: true
            })

            clearInterval(poll)
            resolve(null)

        } catch (e) {
            logger.warn("Can't connect to MongoDB, Trying again in 5 sec")
        }

    }, 5000)
})



export const seedGeneralManager = async () => {
    const gmEmail = "gm@email.com"

    const gmUser = await getUserByEmail(gmEmail)
    if (!gmUser)
        await createUser(gmEmail, {
            [USER_TYPES.GENERAL_MANAGER]: true
        })
}
