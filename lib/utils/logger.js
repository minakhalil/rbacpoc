import winston from 'winston'
import { MainConfig } from '../config';


const { combine, timestamp, colorize, errors, printf } = winston.format

var logger = winston.createLogger({
    level: MainConfig.logLevel,
    format: combine(
        errors({ stack: true }),
        colorize(),
        timestamp(),
        printf(info => {
            return `${info.timestamp} [${info.level}] : ${JSON.stringify(info.message)}`
        })
    ),
    transports: [
        new winston.transports.Console()
    ],
    exitOnError: false,
});


export {
    logger
}