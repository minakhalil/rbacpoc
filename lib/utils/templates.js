import { MainConfig } from "../config"

/**
 * 
 * Templates for generated strings in (Emails,..)
 *  ** Can be replaced by a html engine
 * 
 */
export const EmailChangeTemplate = (oldEmail, newEmail) => {
    return {
        subject: `Email Has Been Changed!`,
        body: `Your email has been changed from ${oldEmail} to ${newEmail}`
    }
}

