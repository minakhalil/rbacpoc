import { MainConfig } from "../config";

const swaggerSpecOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: MainConfig.serviceName,
            version: "1.0.0",
            description:
                "POC for RBAC",

        },
        servers: [
            {
                url: `http://${MainConfig.hostedURL}:${MainConfig.port}`,
            },
        ],
    },
    apis: [
        `${__dirname}/../../server/router.js`,
        `${__dirname}/../modules/auth/auth.router.js`,
        `${__dirname}/../modules/user/user.router.js`,
        `${__dirname}/../modules/group/group.router.js`,
        `${__dirname}/../modules/collection/collection.router.js`,
        `${__dirname}/../modules/item/item.router.js`,
    ],
};

export {
    swaggerSpecOptions
}