/**
 * CustomError
 * 
 * Class to handle all api errors thrown by controllers
 *    
 */
export class CustomError extends Error {
    msg = ""
    code = 0
    status = 422
    
    constructor(msg, code = 0, status = 422) {
        super()

        this.msg = msg
        this.status = status
        this.code = code
    }

}