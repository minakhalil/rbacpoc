import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { createUser } from '../modules/user/user.service';
import request from 'supertest'
import { app } from '../../server/app'
import { USER_TYPES } from '../types';

let mongo

beforeAll(async () => {
    mongo = await MongoMemoryServer.create()
    const mongoUri = mongo.getUri()

    await mongoose.connect(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        ignoreUndefined: true
    })

})

beforeEach(async () => {
    jest.clearAllMocks();
    const collections = await mongoose.connection.db.collections()

    for (let collection of collections) {
        await collection.deleteMany({})
    }
    await createGeneralManagerWithMocks()
})


afterAll(async () => {
    await mongo.stop();
    await mongoose.connection.close()
})

const createGeneralManagerWithMocks = async () => {
    const gmEmail = "gm@email.com"

    await createUser(gmEmail, {
        [USER_TYPES.GENERAL_MANAGER]: true
    })
    const resGmLogin = await request(app)
        .post('/api/auth/login')
        .send({
            email: gmEmail
        })

    global.gmAccessToken = resGmLogin.body.accessToken

    const g = await request(app)
        .post('/api/group/')
        .set({ Authorization: global.gmAccessToken })
        .send({
            name: 'G1',
        })

    const c = await request(app)
        .post('/api/collection/')
        .set({ Authorization: global.gmAccessToken })
        .send({
            name: 'C1',
            group: g.body.group._id
        })

    global.mocksObjects = {
        group: g.body.group._id,
        collection: c.body.collection._id
    }


}