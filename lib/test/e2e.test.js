import request from 'supertest'
import { app } from '../../server/app'
import mongoose from 'mongoose'
import { createUser, getAllUsers } from '../../lib/modules/user/user.service'
import { USER_TYPES } from '../types'
import { generateAccessToken } from '../utils/token'

/**
 * 
 *      [E2E] For all RBAC flows
 *          
 *      General Manager GM
 *  
 *      Details:
 *      [1] GM Create 1 Group(G1), 1 Collection (C1), 1 Item (I1) for G1     
 *      [2] GM Create 2 users for G1 , one admin U1, one regular U2
 *      [3] U2 fails to edit any resource but U1 can access all of them
 *      [4] Admin U1 create user U3 regular to group G1 but U2 fails to create U4
 *      [5] GM create G2 and C2, I2 for G2
 *      [6] U1 fails to access to G2 and all related reources
 *      [7] GM add U1 to G2 admins
 *      [8] U1 now can access G1,G2 resources
 *      
 *       
 */

describe("E2E", () => {

    it('Full flow test', async () => {

        const gmAccessToken = global.gmAccessToken

        /**
         *  [1] GM Create 1 Group(G1), 1 Collection (C1), 1 Item (I1) for G1
         */

        const resG1 = await request(app)
            .post('/api/group/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'G1',
            })
            .expect(201)
        const G1 = resG1.body.group._id

        const resC1 = await request(app)
            .post('/api/collection/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'C1',
                group: G1
            })
            .expect(201)

        const C1 = resC1.body.collection._id


        const resI1 = await request(app)
            .post('/api/item/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'I1',
                collection: C1
            })
            .expect(201)

        const I1 = resI1.body.item._id


        /**
         *  [2] GM Create 2 users for G1 , one admin U1, one regular U2
         */

        const [resU1, resU2] = await Promise.all([
            request(app)
                .post('/api/user/')
                .set({ Authorization: gmAccessToken })
                .send({
                    email: 'u1@email.com',
                    newRoles: {
                        [USER_TYPES.GROUP_MANAGER]: [G1.toString()]
                    }
                })
                .expect(201),

            request(app)
                .post('/api/user/')
                .set({ Authorization: gmAccessToken })
                .send({
                    email: 'u2@email.com',
                    newRoles: {
                        [USER_TYPES.REGULAR]: [G1.toString()]
                    }
                })
                .expect(201)
        ])


        const [U1, U2] = [resU1.body.user._id, resU2.body.user._id]


        const U1AccessToken = generateAccessToken({ uid: U1.toString() })
        const U2AccessToken = generateAccessToken({ uid: U2.toString() })


        /**
         *  [3] U2 fails to edit any resource but U1 can access all of them
         */

        await Promise.all([


            //both can read
            request(app)
                .get(`/api/group/${G1}`)
                .set({ Authorization: U1AccessToken })
                .expect(200),

            request(app)
                .get(`/api/group/${G1}`)
                .set({ Authorization: U2AccessToken })
                .expect(200),



            //only admin can edit
            request(app)
                .put(`/api/item/${I1}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'I1' })
                .expect(204),

            request(app)
                .put(`/api/item/${I1}`)
                .set({ Authorization: U2AccessToken })
                .send({ name: 'I1' })
                .expect(403),

            request(app)
                .put(`/api/collection/${C1}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'C1' })
                .expect(204),

            request(app)
                .put(`/api/collection/${C1}`)
                .set({ Authorization: U2AccessToken })
                .send({ name: 'C1' })
                .expect(403)

        ])

        /**
         *  [4] Admin U1 create user U3 regular to group G1 but U2 fails to create U4
         */

        await Promise.all([

            request(app)
                .post('/api/user/')
                .set({ Authorization: U1AccessToken })
                .send({
                    email: 'u3@email.com',
                    newRoles: {
                        [USER_TYPES.REGULAR]: [G1.toString()]
                    }
                })
                .expect(201),

            request(app)
                .post('/api/user/')
                .set({ Authorization: U2AccessToken })
                .send({
                    email: 'u4@email.com',
                    newRoles: {
                        [USER_TYPES.REGULAR]: [G1.toString()]
                    }
                })
                .expect(403)

        ])

        /**
         *  [5] GM create G2 and C2, I2 for G2
         */

        const resG2 = await request(app)
            .post('/api/group/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'G2',
            })
            .expect(201)
        const G2 = resG2.body.group._id

        const resC2 = await request(app)
            .post('/api/collection/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'C2',
                group: G2
            })
            .expect(201)

        const C2 = resC2.body.collection._id


        const resI2 = await request(app)
            .post('/api/item/')
            .set({ Authorization: gmAccessToken })
            .send({
                name: 'I2',
                collection: C2
            })
            .expect(201)

        const I2 = resI2.body.item._id



        /**
         * [6] U1 fails to access to G2 and all related reources
         */

        await Promise.all([

            request(app)
                .get(`/api/group/${G2}`)
                .set({ Authorization: U1AccessToken })
                .expect(403),


            request(app)
                .put(`/api/item/${I2}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'I2' })
                .expect(403),


            request(app)
                .put(`/api/collection/${C2}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'C2' })
                .expect(403)
        ])


        /**
         * [7] GM add U1 to G2 admins
         */
        await request(app)
            .put(`/api/user/${U1}`)
            .set({ Authorization: gmAccessToken })
            .send({
                newRoles: {
                    [USER_TYPES.GROUP_MANAGER]: [G2.toString()]
                }
            })
            .expect(204)



        /**
        * [8] U1 now can access G1,G2 resources
        */

        await Promise.all([
            request(app)
                .get(`/api/group/${G2}`)
                .set({ Authorization: U1AccessToken })
                .expect(200),


            request(app)
                .put(`/api/item/${I2}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'I2' })
                .expect(204),


            request(app)
                .put(`/api/collection/${C2}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'C2' })
                .expect(204),

            request(app)
                .get(`/api/group/${G1}`)
                .set({ Authorization: U1AccessToken })
                .expect(200),


            request(app)
                .put(`/api/item/${I1}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'I1' })
                .expect(204),


            request(app)
                .put(`/api/collection/${C1}`)
                .set({ Authorization: U1AccessToken })
                .send({ name: 'C1' })
                .expect(204)

        ])

    })
})


